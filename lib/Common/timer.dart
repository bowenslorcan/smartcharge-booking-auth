/*
Title: Building a Countdown Timer with a Custom Painter and Animations in Dart's Flutter Framework
Author: Tensor Programming
Date: Mar 25, 2018
Code version: 1
Availability: https://www.youtube.com/watch?v=tRe8teyf9Nk
*/

import '../imports.dart';
import 'dart:math' as math;

class TimerScreen extends StatefulWidget {
  final duration;

  TimerScreen({this.duration});

  @override
  _TimerScreenState createState() => _TimerScreenState();
}

class _TimerScreenState extends State<TimerScreen>
    with TickerProviderStateMixin {
  AnimationController controller;

  String _message;

  String get timerString {
    Duration duration = controller.duration * controller.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  final String chargePointID = 'CP:C65QJ';

  CrudServices _servicesObj = new CrudServices();

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: widget.duration),
    );
    controller.reverse(from: controller.value == 0.0 ? 1.0 : controller.value);
    _servicesObj.updatePointStatus(chargePointID, true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Align(
              alignment: FractionalOffset.center,
              child: AspectRatio(
                aspectRatio: 1.0,
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width - 120.0,
                      height: MediaQuery.of(context).size.width - 120.0,
                      child: AnimatedBuilder(
                        animation: controller,
                        builder: (BuildContext context, Widget child) {
                          return new CustomPaint(
                            painter: TimerPainter(
                                animation: controller,
                                backgroundColor:
                                    Color(0xFF0D2A47).withOpacity(0.1),
                                color: Color(0xFF4ABD61)),
                          );
                        },
                      ),
                    ),
                    Align(
                      alignment: FractionalOffset.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FadeAnimation(
                            0.5,
                            Container(
                              width: 80.0,
                              height: 167.0,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                          'assets/images/charge_point.png'))),
                            ),
                          ),
                          FadeAnimation(
                            1.0,
                            AnimatedBuilder(
                              animation: controller,
                              builder: (BuildContext context, Widget child) {
                                return new Text(
                                  timerString,
                                  style: TextStyle(
                                      fontSize: 75.0,
                                      fontFamily: 'Monstserrat',
                                      color: Colors.black38,
                                      fontWeight: FontWeight.w200),
                                );
                              },
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            FadeAnimation(
              1.5,
              Container(
                width: 250.0,
                child: FlatButton(
                  child: Text(
                    'Stop Charge',
                    style: TextStyle(
                        fontSize: 16.0,
                        fontFamily: 'Monstserrat',
                        fontWeight: FontWeight.w700),
                  ),
                  shape: new RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(color: Colors.red),
                  ),
                  color: Colors.red,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(20.0),
                  onPressed: () {
                    if (controller.isAnimating) {
                      controller.stop();
                      _servicesObj.updatePointStatus(chargePointID, false);
                      Navigator.pushReplacementNamed(context, '/');
                      setState(() {
                        _message =
                            "Charge complete, thank you for using SmartCharge 😁";
                        return showSuccessFlushbar(context, _message);
                      });
                    } else {
                      _servicesObj.updatePointStatus(chargePointID, false);
                      Navigator.pushReplacementNamed(context, '/');
                      setState(() {
                        _message =
                            "Charge complete, thank you for using SmartCharge 😁";
                        return showSuccessFlushbar(context, _message);
                      });
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TimerPainter extends CustomPainter {
  TimerPainter({this.animation, this.backgroundColor, this.color})
      : super(repaint: animation);

  final Animation<double> animation;
  final Color backgroundColor, color;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = backgroundColor
      ..strokeWidth = 5.0
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);
    paint.color = color;
    double progress = (1.0 - animation.value) * 2 * math.pi;
    canvas.drawArc(Offset.zero & size, math.pi * 1.5, -progress, false, paint);
  }

  @override
  bool shouldRepaint(TimerPainter old) {
    return animation.value != old.animation.value ||
        color != old.color ||
        backgroundColor != old.backgroundColor;
  }
}
