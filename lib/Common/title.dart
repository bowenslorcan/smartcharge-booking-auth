import '../imports.dart';

class TitleContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 100.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Smart',
            style: TextStyle(
                fontSize: 48.0,
                letterSpacing: 1.0,
                fontWeight: FontWeight.w700,
                color: Color(0xFF4ABD61)),
          ),
          Text(
            'Charge',
            style: TextStyle(
                fontSize: 48.0,
                letterSpacing: 1.0,
                fontWeight: FontWeight.w700,
                color: Color(0xFF0D2A47)),
          )
        ],
      ),
    );
  }
}
