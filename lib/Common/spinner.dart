import '../imports.dart';

class Spinner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SpinKitCircle(
        color: Color(0xFF4ABD61),
        size: 50.0,
      ),
    );
  }
}
