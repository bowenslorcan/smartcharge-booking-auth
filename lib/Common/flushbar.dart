import '../imports.dart';

void showSuccessFlushbar(BuildContext context, message) {
  Flushbar(
    title: 'Success',
    message: message,
    icon: Icon(
      Icons.check_circle_outline,
      size: 28,
      color: Color(0xFF4ABD61),
    ),
    leftBarIndicatorColor: Color(0xFF4ABD61),
    duration: Duration(seconds: 5),
  )
    ..show(context);
}

void showErrorFlushbar(BuildContext context, String message) {
  Flushbar(
    title: 'Error',
    message: message,
    icon: Icon(
      Icons.info_outline,
      size: 28,
      color: Color(0xffff9966),
    ),
    leftBarIndicatorColor: Color(0xffff9966),
    duration: Duration(seconds: 5),
  )
    ..show(context);
}