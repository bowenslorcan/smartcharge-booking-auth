import '../imports.dart';

class DurationSelectorPage extends StatefulWidget {
  @override
  _DurationSelectorPageState createState() => _DurationSelectorPageState();
}

class _DurationSelectorPageState extends State<DurationSelectorPage> {
  String _message;

  final String chargePointID = 'CP:C65QJ';

  final _duration = new TextEditingController();

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  CrudServices _servicesObj = CrudServices();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      body: Form(
          key: _formKey,
          child: Stack(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TitleContainer(),
                    Column(
                      children: <Widget>[
                        FadeAnimation(
                          1.0,
                          Container(
                            height: 60.0,
                            width: 250.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(color: Color(0xFF2196F3)),
                                color: Colors.white),
                            child: Center(
                              child: TextFormField(
                                controller: _duration,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                    fontFamily: 'Monstserrat',
                                    fontWeight: FontWeight.w300),
                                keyboardType: TextInputType.number,
                                validator: Validators.compose([
                                  Validators.required(
                                      'Please enter a duration'),
                                  Validators.minLength(
                                      2, 'At least 10 minutes'),
                                  Validators.maxLength(3, '6 hours max'),
                                  Validators.patternString(
                                      r"^[0-9]+$", 'Only numbers are allowed'),
                                ]),
                                decoration: InputDecoration(
                                  hintText: 'Duration in minutes...',
                                  hintStyle: TextStyle(
                                      color: Colors.black45,
                                      fontSize: 16.0,
                                      fontFamily: 'Monstserrat',
                                      fontWeight: FontWeight.w300),
                                  border: InputBorder.none,
                                  contentPadding:
                                      EdgeInsets.only(left: 15.0, top: 15.0),
                                  suffixIcon: Icon(
                                    Icons.alarm,
                                    color: Color(0xFF2196F3),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 60.0,
                        ),
                        FadeAnimation(
                          1.5,
                          Container(
                            width: 250.0,
                            child: FlatButton(
                              child: Text(
                                'Confirm',
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontFamily: 'Monstserrat',
                                    fontWeight: FontWeight.w700),
                              ),
                              shape: new RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                side: BorderSide(color: Color(0xFF4ABD61)),
                              ),
                              color: Color(0xFF4ABD61),
                              textColor: Colors.white,
                              padding: EdgeInsets.all(20.0),
                              onPressed: () {
                                _submitCommand(_duration.text);
                              },
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }

  void _submitCommand(duration) {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      _validateDuration(duration);
    }
  }

  Future<void> _validateDuration(duration) async {
    DateTime timeNow = new DateTime.now();
    String bookingStartTime;
    DateTime bookingStartDateTime;
    int startTimeValidator;
    int durationAsInt = int.parse(duration);
    // Converting minutes to seconds to be passed to timmer
    int timerDuration = durationAsInt * 60;

    if (durationAsInt > 360) {
      setState(() {
        _message =
            'This is a Standard Type 2 point, you cannot have a booking greater than 6 hours!';
        return showErrorFlushbar(context, _message);
      });
    } else {
      _servicesObj.getBookings(chargePointID).then((QuerySnapshot bookings) {
        if (bookings.documents.isNotEmpty) {
          bookingStartTime = bookings.documents[0].data['start_time'];
          bookingStartDateTime =
              bookings.documents[0].data['start_time_date'].toDate();

          Duration startTimeDifference =
              timeNow.difference(bookingStartDateTime);
          startTimeValidator = startTimeDifference.inMinutes;

          if (durationAsInt > startTimeValidator) {
            setState(() {
              _message =
                  "We're sorry, there is a booking at this point from $bookingStartTime!";
              return showErrorFlushbar(context, _message);
            });
          } else {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => TimerScreen(duration: timerDuration),
            ));
            setState(() {
              _message = "Your booking has started!";
              return showSuccessFlushbar(context, _message);
            });
          }
        } else {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => TimerScreen(duration: timerDuration),
          ));
          setState(() {
            _message = "Your booking has started!";
            return showSuccessFlushbar(context, _message);
          });
        }
      });
    }
  }
}
