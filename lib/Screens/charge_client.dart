import '../imports.dart';

class ChargeClientPage extends StatefulWidget {
  @override
  _ChargeClientPageState createState() => _ChargeClientPageState();
}

class _ChargeClientPageState extends State<ChargeClientPage> {
  bool _idle = true;
  bool _active = false;
  String _message;

  CrudServices _servicesObj = CrudServices();

  final _ecard = new TextEditingController();

  final String chargePointID = 'CP:C65QJ';

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      body: Form(
          key: _formKey,
          child: Stack(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FadeAnimation(0.5, TitleContainer()),
                    Visibility(
                        visible: _idle,
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              _idle = !_idle;
                              _active = !_active;
                            });
                          },
                          child: Column(
                            children: <Widget>[
                              FadeAnimation(
                                1.0,
                                Container(
                                  width: 114.0,
                                  height: 238.0,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: AssetImage(
                                              'assets/images/charge_point.png'))),
                                ),
                              ),
                              SizedBox(height: 10.0),
                              FadeAnimation(
                                1.5,
                                Text(
                                  'Tap to Start',
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.black,
                                      fontFamily: 'Monstserrat',
                                      fontWeight: FontWeight.w400),
                                ),
                              )
                            ],
                          ),
                        )),
                    Visibility(
                      visible: _active,
                      child: Column(
                        children: <Widget>[
                          FadeAnimation(
                            1.0,
                            Container(
                              width: 250.0,
                              child: Center(
                                child: FlatButton(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Tap Ecard',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontFamily: 'Monstserrat',
                                            fontWeight: FontWeight.w500),
                                      ),
                                      SizedBox(
                                        width: 5.0,
                                      ),
                                      Container(
                                        height: 30.0,
                                        width: 30.0,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage(
                                                    'assets/images/nfc_icon.png'))),
                                      ),
                                    ],
                                  ),
                                  shape: new RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    side: BorderSide(color: Color(0xFF2196F3)),
                                  ),
                                  color: Colors.white,
                                  textColor: Color(0xFF2196F3),
                                  padding: EdgeInsets.all(15.0),
                                  onPressed: () {
                                    _readCard();
                                  },
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Center(
                            child: Text(
                              'OR',
                              style: TextStyle(
                                  color: Colors.black26,
                                  fontSize: 16.0,
                                  fontFamily: 'Monstserrat',
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          FadeAnimation(
                            1.5,
                            Container(
                              width: 250.0,
                              child: TextFormField(
                                controller: _ecard,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                    fontFamily: 'Monstserrat',
                                    fontWeight: FontWeight.w500),
                                keyboardType: TextInputType.number,
                                obscureText: true,
                                validator: Validators.compose([
                                  Validators.minLength(
                                      7, 'Your ecard must be 7 digits'),
                                  Validators.maxLength(
                                      7, 'Your ecard must be 7 digits'),
                                  Validators.patternString(
                                      r"^[0-9]+$", 'Only numbers are allowed'),
                                ]),
                                decoration: InputDecoration(
                                    suffixIcon: Container(
                                      height: 30.0,
                                      width: 30.0,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'assets/images/keypad_icon_resize.png'))),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.black),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xFF2196F3))),
                                    hintText: 'Ecard Number',
                                    hintStyle: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontFamily: 'Monstserrat',
                                        fontWeight: FontWeight.w300)),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 60.0,
                          ),
                          FadeAnimation(
                            2.0,
                            Container(
                              width: 250.0,
                              child: FlatButton(
                                child: Text(
                                  'Start Charge',
                                  style: Constants.buttonTextStyle(),
                                ),
                                shape: new RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  side: BorderSide(color: Color(0xFF4ABD61)),
                                ),
                                color: Color(0xFF4ABD61),
                                textColor: Colors.white,
                                padding: EdgeInsets.all(20.0),
                                onPressed: () {
                                  _submitCommand(_ecard.text);
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }

  /*
    Title: flutter_nfc_reader
    Author: Crippa Matteo
    Date: Oct 13, 2019
    Code version: 0.1.0
    Availability: https://pub.dev/packages/flutter_nfc_reader
  */

  void _submitCommand(ecard) {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      _validateBooking(ecard);
    }
  }

  _readCard() async {
    setState(() {
      _message = 'Ready to scan!';
      return showSuccessFlushbar(context, _message);
    });
    NDEFMessage message = await NFC.readNDEF(once: true).first.then((val) {
      _ecard.text = val.id;
      _validateBooking(_ecard.text);
      return;
    });
    print("payload: ${message.id}");
  }

  Future<void> _validateBooking(ecard) async {
    DateTime timeNow = new DateTime.now();
    String ecardReturned, bookingStartTime;
    DateTime bookingStartDateTime, bookingEndDateTime;
    int startTimeValidator,
        startTimeValidatorSecs,
        bookingDurationValidator,
        endTimeValidator,
        endTimeValidatorSecs;

    if (ecard == '') {
      setState(() {
        _message = 'Please scan or enter your ecard number!';
        return showErrorFlushbar(context, _message);
      });
    } else {
      try {
        _servicesObj.getBookings(chargePointID).then((QuerySnapshot bookings) {
          if (bookings.documents.isNotEmpty) {
            // Decrypt ecard
            final decryptedEcard =
                AesService().decrypter(bookings.documents[0].data['ecard']);
            ecardReturned = decryptedEcard.toString();

            bookingStartTime = bookings.documents[0].data['start_time'];
            bookingStartDateTime =
                bookings.documents[0].data['start_time_date'].toDate();
            bookingEndDateTime =
                bookings.documents[0].data['end_time_date'].toDate();

            Duration bookingDuration = bookingEndDateTime.difference(timeNow);
            bookingDurationValidator = bookingDuration.inSeconds;

            Duration startTimeDifference =
                timeNow.difference(bookingStartDateTime);
            startTimeValidator = startTimeDifference.inMinutes;
            startTimeValidatorSecs = startTimeDifference.inSeconds;

            if (startTimeValidator >= -10 && startTimeValidator < 10) {
              if (ecardReturned == ecard) {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) =>
                      TimerScreen(duration: bookingDurationValidator),
                ));
              } else {
                setState(() {
                  _message =
                      "We're sorry, there's a booking here at $bookingStartTime!";
                  return showErrorFlushbar(context, _message);
                });
              }
            } else {
              if (startTimeValidator < -10) {
                if (ecardReturned == ecard) {
                  _message =
                      "Looks like you're early for your booking. Would you like to start now?";
                  _showDialog(_message, bookingDurationValidator);
                } else {
                  _message =
                      "There's a booking here at $bookingStartTime. You can still charge until then!";
                  _showDialog(_message, startTimeValidatorSecs);
                }
              } else {
                Duration startTimeDifference =
                    bookingEndDateTime.difference(timeNow);
                endTimeValidator = startTimeDifference.inMinutes;
                endTimeValidatorSecs = startTimeDifference.inSeconds;

                print(endTimeValidator);

                if (endTimeValidator > 5) {
                  if (ecardReturned == ecard) {
                    _message =
                        "Looks like you're late for your booking. Would you like to finish it now?";
                    _showDialog(_message, endTimeValidatorSecs);
                  } else {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => DurationSelectorPage(),
                    ));
                  }
                } else {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => DurationSelectorPage(),
                  ));
                }
              }
            }
          } else {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => DurationSelectorPage(),
            ));
          }
        });
      } catch (e) {
        print(e);
        setState(() {
          _message = "Something went wrong, please try again!";
          return showErrorFlushbar(context, _message);
        });
      }
    }
  }

  void _showDialog(String content, int duration) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Continue Charge'),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text('Start Charge'),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => TimerScreen(duration: duration),
                  ));
                },
              )
            ],
          );
        });
  }
}
