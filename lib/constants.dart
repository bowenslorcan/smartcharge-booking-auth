import 'imports.dart';

class Constants {
  static buttonTextStyle() {
    return TextStyle(
        fontSize: 16.0, fontFamily: 'Monstserrat', fontWeight: FontWeight.w700);
  }
}
