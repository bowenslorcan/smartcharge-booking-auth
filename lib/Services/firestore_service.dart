/*
Title: Flutter - Firestore CRUD (Reading and writing)
Author: Raja Yogan
Date: Sep 6, 2018
Code version: 1
Availability: https://www.youtube.com/watch?v=ipa3T_gVe8U
*/

import '../imports.dart';

class CrudServices {
  final _db = Firestore.instance;

  getBookings(chargePointID) {
    return _db
        .collection('bookings')
        .where('charge_point', isEqualTo: chargePointID)
        .getDocuments();
  }

  updatePointStatus(chargePointID, newStatus) {
    _db
        .collection('charge_points')
        .document(chargePointID)
        .updateData({'occupied': newStatus});
  }
}
