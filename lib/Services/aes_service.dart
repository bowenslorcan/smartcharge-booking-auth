import 'package:encrypt/encrypt.dart' as aes;

class AesService {
  final key = aes.Key.fromLength(32);
  final iv = aes.IV.fromLength(16);

  decrypter(data) {
    final encrypter = aes.Encrypter(aes.AES(key));
    return encrypter.decrypt64(data, iv: iv);
  }
}