import 'imports.dart';

void main() => runApp(SmartCharge());

class SmartCharge extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "SmartCharge",
      theme: ThemeData(
        fontFamily: 'Montserrat',
        primarySwatch: Colors.green,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => ChargeClientPage()
      },
    );
  }
}
